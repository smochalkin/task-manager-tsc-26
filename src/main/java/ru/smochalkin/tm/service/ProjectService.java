package ru.smochalkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.repository.IProjectRepository;
import ru.smochalkin.tm.api.service.IProjectService;
import ru.smochalkin.tm.exception.empty.EmptyNameException;
import ru.smochalkin.tm.model.Project;

public final class ProjectService extends AbstractBusinessService<Project> implements IProjectService {

    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
    }

    @Override
    @NotNull
    public Project create(@NotNull final String userId,
                          @Nullable final String name,
                          @Nullable final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        businessRepository.add(project);
        return project;
    }

}
