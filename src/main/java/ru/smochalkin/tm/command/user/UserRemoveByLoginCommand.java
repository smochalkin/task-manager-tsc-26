package ru.smochalkin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.util.TerminalUtil;

public class UserRemoveByLoginCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "user-remove-by-login";
    }

    @Override
    @NotNull
    public String description() {
        return "Remove a user by login.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        serviceLocator.getAuthService().getUserId();
        System.out.println("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeByLogin(login);
    }

    @Override
    @NotNull
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
