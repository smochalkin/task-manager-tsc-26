package ru.smochalkin.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.service.ServiceLocator;
import ru.smochalkin.tm.enumerated.Role;

public abstract class AbstractCommand {

    @Nullable
    protected ServiceLocator serviceLocator;

    public void setServiceLocator(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    public abstract String name();

    @Nullable
    public abstract String description();

    public abstract void execute();

    @Nullable
    public Role[] roles(){
        return null;
    }

    @Override
    @NotNull
    public String toString() {
        @NotNull String result = "";
        @Nullable final String name = name();
        @Nullable final String description = description();
        if(name != null && !name.isEmpty()) result += name + " ";
        if(description != null && !description.isEmpty()) result += ": " + description;
        return result;
    }

}
